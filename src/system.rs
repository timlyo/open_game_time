use std::fs;

#[derive(Debug)]
pub struct Process {
    pid: usize
}

pub fn get_list_of_processes() -> Vec<Process> {
    fs::read_dir("/proc/").expect("Can't read proc")
        .filter(|file| file.is_ok())
        .map(|file| file.unwrap())
        .map(|file| {
            // "null" will be ignored below as it wont parse into a usize
            file.file_name().into_string().unwrap_or("null".into())
        })
        .filter_map(|file| {
            match file.parse::<usize>() {
                Ok(num) => Some(num),
                Err(_) => None
            }
        }).map(|pid| {
        Process {
            pid
        }
    }).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_list_of_processes() {
        let processes = get_list_of_processes();
        println!("{:?}", processes);
    }
}